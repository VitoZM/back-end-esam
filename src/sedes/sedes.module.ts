import { Module } from '@nestjs/common';
import { SedesController } from './sedes.controller';
import { SedesService } from './sedes.service';
import { SedesResolver } from './sedes.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { sedes } from './sedes.entity';

@Module({
  imports: [TypeOrmModule.forFeature([sedes])],
  controllers: [SedesController],
  providers: [SedesService, SedesResolver]
})
export class SedesModule {}
