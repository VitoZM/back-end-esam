import { Resolver, Query, Args, Int } from '@nestjs/graphql';
import { sedes } from './sedes.entity';
import { SedesService } from './sedes.service';

@Resolver((of) => sedes)
export class SedesResolver {
  constructor(private SedesService: SedesService) {}

  @Query((returns) => [sedes])
  async sedes(): Promise<sedes[]> {
    return this.SedesService.read();
  }

  @Query((returns) => [sedes])
  async sedesByUnidadNegocio(
    @Args('estado', { type: () => Int }) estado: number,
    @Args('unidad_negocio', { type: () => Int }) unidad_negocio: number,
  ): Promise<sedes[]> {
    return this.SedesService.findById(estado, unidad_negocio);
  }
}
