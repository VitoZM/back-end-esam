import { Field, Int, ObjectType, Float } from "@nestjs/graphql";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
@ObjectType()
export class sedes{
    @PrimaryGeneratedColumn()
    @Field(type => Int)
    id: number;

    @Column()
    @Field()
    nombre: string;

    @Column()
    @Field()
    direccion: string;

    @Column()
    @Field()
    telefono_fijo: string;

    @Column()
    @Field()
    telefono1: string;

    @Column()
    @Field()
    telefono2: string;

    @Column()
    @Field(type => Int)
    estado: number;

    @Column()
    @Field(type => Float)
    latitud: number;

    @Column()
    @Field(type => Float)
    longitud: number;

    @Column()
    @Field(type => Int)
    idciudad: number;

    @Column()
    @Field(type => Int)
    unidad_negocio: number;

    @Column()
    @Field()
    modelo_recibo: string;
    
}