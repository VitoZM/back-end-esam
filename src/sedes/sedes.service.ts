import { Injectable, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { sedes } from './sedes.entity';

@Injectable()
export class SedesService {

    constructor(@InjectRepository(sedes) private sedesRepository:Repository<sedes>){}

    async read(): Promise<sedes[]>{
        return this.sedesRepository.find();
    }
    
    async findById(estado: number, unidad_negocio: number): Promise<sedes[]>{
        return this.sedesRepository.find({ where: { unidad_negocio,estado } });
    }
}
