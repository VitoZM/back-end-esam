import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UnidadNegocioModule } from './unidad-negocio/unidad-negocio.module';
import { SedesModule } from './sedes/sedes.module';
import { ProgramasModule } from './programas/programas.module';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '66.42.76.192',
      port: 3306,
      username: 'esam',
      password: 'esam-user-test',
      database: 'adminesamdb',
      entities: ['src/entity/*.ts', 'dist/**/*.entity{.ts,.js}'],
      synchronize: false,
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '66.42.76.192',
      port: 3306,
      name: 'academicoesamdb',
      username: 'esam',
      password: 'esam-user-test',
      database: 'academicoesamdb',
      entities: ['src/entity/*.ts', 'dist/**/*.entity{.ts,.js}'],
      synchronize: false,
    }),
    UnidadNegocioModule,
    SedesModule,
    ProgramasModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
