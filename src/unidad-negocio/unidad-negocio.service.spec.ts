import { Test, TestingModule } from '@nestjs/testing';
import { UnidadNegocioService } from './unidad-negocio.service';

describe('UnidadNegocioService', () => {
  let service: UnidadNegocioService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UnidadNegocioService],
    }).compile();

    service = module.get<UnidadNegocioService>(UnidadNegocioService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
