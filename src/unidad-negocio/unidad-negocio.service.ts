import { Injectable, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { unidad_negocio } from './unidad-negocio.entity';

@Injectable()
export class UnidadNegocioService {

    constructor(@InjectRepository(unidad_negocio) private unidadNegocioRepository:Repository<unidad_negocio>){}
    async read(estado): Promise<unidad_negocio[]>{
        return this.unidadNegocioRepository.find({where:{estado}});
    }
    
}
