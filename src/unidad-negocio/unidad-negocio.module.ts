import { Module } from '@nestjs/common';
import { UnidadNegocioController } from './unidad-negocio.controller';
import { UnidadNegocioService } from './unidad-negocio.service';
import { UnidadNegocioResolver } from './unidad-negocio.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { unidad_negocio } from './unidad-negocio.entity';

@Module({
  imports: [TypeOrmModule.forFeature([unidad_negocio])],
  controllers: [UnidadNegocioController],
  providers: [UnidadNegocioService, UnidadNegocioResolver]
})
export class UnidadNegocioModule {}
