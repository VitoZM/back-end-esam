import { Test, TestingModule } from '@nestjs/testing';
import { UnidadNegocioController } from './unidad-negocio.controller';

describe('UnidadNegocioController', () => {
  let controller: UnidadNegocioController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UnidadNegocioController],
    }).compile();

    controller = module.get<UnidadNegocioController>(UnidadNegocioController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
