import { Field, Int, ObjectType } from "@nestjs/graphql";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
@ObjectType()
export class unidad_negocio{
    @PrimaryGeneratedColumn()
    @Field(type => Int)
    id: number;

    @Column()
    @Field()
    nombre: string;

    @Column()
    @Field()
    codigo: string;

    @Column()
    @Field()
    descripcion: string;

    @Column()
    @Field()
    email: string;

    @Column()
    @Field()
    sitio_web: string;

    @Column()
    @Field(type => Int)
    estado: number;
    
}