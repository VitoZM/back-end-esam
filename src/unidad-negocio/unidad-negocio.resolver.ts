import { Resolver, Query, Int, Args } from '@nestjs/graphql';
import { unidad_negocio } from './unidad-negocio.entity';
import { UnidadNegocioService } from './unidad-negocio.service';

@Resolver((of) => unidad_negocio)
export class UnidadNegocioResolver {
  constructor(private UnidadNegocioService: UnidadNegocioService) {}

  @Query((returns) => [unidad_negocio])
  async unidad_negocios(@Args('estado', { type: () => Int }) estado: number): Promise<unidad_negocio[]> {
    return this.UnidadNegocioService.read(estado);
  }
}
