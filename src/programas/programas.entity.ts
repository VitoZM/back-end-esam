import { Field, Int, ObjectType, ID } from '@nestjs/graphql';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
@ObjectType()
export class programas {
  @PrimaryGeneratedColumn()
  @Field((type) => ID, { nullable: true })
  id?: string;

  @Column()
  @Field((type) => Int, { nullable: true })
  idpostgrado: number;

  @Column()
  @Field((type) => Int)
  version: number;

  @Column()
  @Field((type) => Int)
  grupo: number;

  @Column()
  @Field((type) => Int)
  idsede: number;

  @Column()
  @Field((type) => Int)
  iduniversidad: number;

  @Column()
  @Field((type) => Int)
  gestion: number;

  @Column()
  @Field((type) => Date)
  fecha_registro: string;

  @Column()
  @Field((type) => Date)
  fecha_inicio: string;

  @Column()
  @Field((type) => Date)
  fecha_fin: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  nombre_compuesto?: string;

  @Column()
  @Field({ nullable: true })
  codigo: string;

  @Column()
  @Field()
  fase: string;

  @Column()
  @Field((type) => Int)
  estado: number;

  @Column()
  @Field((type) => Int)
  estado_ins: number;

  @Column()
  @Field((type) => Int)
  cantidad_ins: number;
}
