import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { programas } from './programas.entity';

@Injectable()
export class ProgramasService {
  constructor(
    @InjectRepository(programas, 'academicoesamdb')
    private programasRepository: Repository<programas>,
  ) {}

  async read(): Promise<programas[]> {
    return this.programasRepository.find();
  }

  async readProgramasEstadoByFecha(estado:number,initialDate:string,finalDate:string,date:string,idsede:number): Promise<programas[]> {
      return this.programasRepository.query(`SELECT P.id, MIN(P.id) AS 'min-id',P.nombre_compuesto,P.fecha_registro,PF.fase, P.codigo
      FROM programas P INNER JOIN programa_fase PF ON P.id = PF.programa_id
      WHERE (PF.fecha_registro <= STR_TO_DATE(?, '%Y-%m-%d') AND DATEDIFF(STR_TO_DATE(?, '%Y-%m-%d'),PF.fecha_registro) >= 0)
            AND P.idsede = ?
            AND P.estado = ?
            AND (P.fecha_registro BETWEEN ? AND ?)
      GROUP BY P.id,P.nombre_compuesto,P.fecha_registro,PF.fase, P.codigo`,[date,date,idsede,estado,initialDate,finalDate]);
  }

  async readProgramasBySede(estado:number,idsede:number,initialDate:string,finalDate:string){
    return this.programasRepository.query(`SELECT * FROM programas WHERE estado= ? AND idsede= ? AND fecha_registro BETWEEN ? AND ?`,[estado,idsede,initialDate,finalDate]);
  }

  async readInscritosEstadoByPrograma(estado:number,idprograma:number,estado_ins:number): Promise<programas[]> {
    return this.programasRepository.query(`SELECT P.id,COUNT(I.id) AS 'cantidad_ins',P.nombre_compuesto,P.fecha_registro, P.codigo
    FROM programas P INNER JOIN inscripciones I ON P.id = I.idprograma
    WHERE P.estado = ?
          AND I.estado = ?
          AND P.id = ?
          AND I.estado_ins = ?
    GROUP BY P.id,P.nombre_compuesto,P.fecha_registro, P.codigo`,[estado,estado,idprograma,estado_ins]);
  }
}
