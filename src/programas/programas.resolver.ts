import { Resolver, Query, Args, Int } from '@nestjs/graphql';
import { programas } from './programas.entity';
import { ProgramasService } from './programas.service';


@Resolver()
export class ProgramasResolver {
  constructor(private programasService: ProgramasService) {}

  @Query((returns) => [programas])
  async programas(): Promise<programas[]> {
    return this.programasService.read();
  }

  @Query((returns) => [programas])
  async readProgramasBySede(
    @Args('estado', { type: () => Int }) estado: number,
    @Args('idsede', { type: () => Int }) idsede: number,
    @Args('initialDate') initialDate: string,
    @Args('finalDate') finalDate: string,
  ): Promise<programas[]> {
    return this.programasService.readProgramasBySede(estado,idsede,initialDate,finalDate);
  }

  @Query((returns) => [programas])
  async readProgramasEstadoByFecha(
    @Args('estado', { type: () => Int }) estado: number,
    @Args('initialDate') initialDate: string,
    @Args('finalDate') finalDate: string,
    @Args('date') date: string,
    @Args('idsede', { type: () => Int }) idsede: number,
  ): Promise<programas[]> {
    return this.programasService.readProgramasEstadoByFecha(estado,initialDate,finalDate,date,idsede);
  }

  @Query((returns) => [programas])
  async readInscritosEstadoByPrograma(
    @Args('estado', { type: () => Int }) estado: number,
    @Args('idprograma', { type: () => Int }) idprograma: number,
    @Args('estado_ins', { type: () => Int }) estado_ins: number,
  ): Promise<programas[]> {
    return this.programasService.readInscritosEstadoByPrograma(estado,idprograma,estado_ins);
  }

}
