import { Module } from '@nestjs/common';
import { ProgramasService } from './programas.service';
import { ProgramasResolver } from './programas.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { programas } from './programas.entity';

@Module({
  imports: [TypeOrmModule.forFeature([programas], 'academicoesamdb')],
  providers: [ProgramasResolver, ProgramasService]
})
export class ProgramasModule {}
